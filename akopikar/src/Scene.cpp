#include "Scene.h"


void Scene::LoadTexture(string fname, int id) 
{

    if(!fname.length())
        throw invalid_argument("Texture fname path is empty");

    if(id >= 8)
        throw invalid_argument("Texture Id is too big");

    SDL_Surface* texture1 = IMG_Load(fname.c_str());
    glViewport(0, 0, texture1->w, texture1->h);

    if (texture1 == nullptr) 
        throw invalid_argument("Texture couldn't be loaded");

    glGenTextures(1, &Textures[id]);
    glBindTexture(GL_TEXTURE_2D, Textures[id]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture1->w, texture1->h, 0, GL_RGB, GL_UNSIGNED_BYTE, texture1->pixels);
    SDL_FreeSurface(texture1);
}


void Scene::drawTex(int x, int y, GLuint Texture) {
    if(x >= Columns || x < 0 || y >= Rows || y < 0)
        throw invalid_argument("Grid values are wrong");

    glColor3f(1, 1, 1);
    glBindTexture(GL_TEXTURE_2D, Texture);

    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glBegin(GL_QUADS);

    glTexCoord2f((GLfloat)x, (GLfloat)y);
    glVertex3f((GLfloat)x, (GLfloat)y + 1, 0.0);

    glTexCoord2f((GLfloat)x, (GLfloat)y + 1);
    glVertex3f((GLfloat)x, (GLfloat)y, 0.0);

    glTexCoord2f((GLfloat)x + 1, (GLfloat)y + 1);
    glVertex3f((GLfloat)x + 1, (GLfloat)y, 0.0);

    glTexCoord2f((GLfloat)x + 1, (GLfloat)y);
    glVertex3f((GLfloat)x + 1, (GLfloat)y + 1, 0.0);

    glEnd();
    glDisable(GL_TEXTURE_2D);
}