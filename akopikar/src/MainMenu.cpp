#include "MainMenu.h"

void MainMenu::load() 
{
    LoadTexture("textures/Menu/title.jpg", 0);
    LoadTexture("textures/Menu/play.jpg", 1);
    LoadTexture("textures/Menu/quit.jpg", 2);
    LoadTexture("textures/Menu/continue.jpg", 3);
}

void MainMenu::menu() 
{
    drawBackground();
    drawButtons();
}

void MainMenu::drawBackground() 
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glColor3f(0.796, 0.207, 0.537);

    glBegin(GL_POLYGON);
    glVertex2f(0, 0);
    glVertex2f(Columns, 0);
    glVertex2f(Columns, Rows);
    glVertex2f(0, Rows);
    glEnd();
    glFlush();
}

void MainMenu::drawButtons() 
{
    drawTex(2, 4, Textures[0]);
    drawTex(2, 2, Textures[1]);
    drawTex(2, 1, Textures[2]);
    drawTex(2, 3, Textures[3]);

    glFlush();
}

void MainMenu::mouse(int button, int state, int x, int y) 
{
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) 
    {
        int mousePosX = x / 360;
        int mousePosY = (1000 - y) / 200;

        if (mousePosX == 2 && mousePosY == 2) 
        {
            StartGame();
        }
        else if (mousePosX == 2 && mousePosY == 1) 
        {
            exit(0);
        }
        else if (mousePosX == 2 && mousePosY == 3) 
        {
            LoadGame();
        }
    
    }
}

