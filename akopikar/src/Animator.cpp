#include "Animator.h"

void Animator::setTextures(GLuint texture1, GLuint texture2) {
	tex1 = texture1;
	tex2 = texture2;
}

GLuint Animator::action(bool ifLeft) {
	if (ifLeft == false) {
		return tex1;
	}
	else {
		return tex2;
	}
}