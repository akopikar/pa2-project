#pragma once
#include "Tower.h"

class ConsumingHouse : public Tower 
{
public:
	/// @brief Constructor
	/// @param costs Tower cost
	/// @param changer Tower action value
	/// @param Tex Tower texture
	/// @param x Tower X position
	/// @param y Tower Y position
	ConsumingHouse(int costs, int changer, GLuint Tex, int x, int y) 
	{
		type = "Consuming";
		posX = x;
		posY = y;
		Texture = Tex;
		changeValue = changer;
		cost = costs;
	}

	/// @brief Constructor to load from string
	/// @param str1 String form which to store tower values
	ConsumingHouse(string consumingHouseAsString) 
	{
		stringstream str(consumingHouseAsString);
		type = "Consuming";
		str >> posX;
		str >> posY;
		str >> Texture;
		str >> changeValue;
		str >> cost;
	}

	int action() override 
	{
		return changeValue * -1;
	}

};