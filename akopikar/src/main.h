#pragma once
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include <string>
#include <iostream>

enum class gameState { MainMenu = 1, InGame = 2 };

static gameState GameState = gameState::MainMenu;

/// @brief Method is used to start the game
void StartGame();
/// @brief Method is used to end the game
void EndGame();
/// @brief Method is used to load the game
void LoadGame();

unsigned GetGameTime();
unsigned GetBestGameTime();

void SetGameTime(unsigned);
void SetBestGameTime(unsigned);
