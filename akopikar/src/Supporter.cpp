#include "Supporter.h"	


void Supporter::load() {
	LoadTexture("textures/Animations/char.jpg", 0);
	LoadTexture("textures/Animations/char1.jpg", 1);
	anim.setTextures(Textures[0], Textures[1]);
	swap(false);
}

void Supporter::menu() {
	drawTex(8, 0, tex);
}

void Supporter::swap(bool ifLeft) {
	tex = anim.action(ifLeft);
}
