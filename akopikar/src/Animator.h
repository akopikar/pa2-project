#include <GL/freeglut.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

class Animator {
	GLuint tex1 = 0;
	GLuint tex2 = 0;
public:

	/// @brief Method sets two textures for swapping animation
	/// @param texture1 first image
	/// @param texture2 second image
	void setTextures(GLuint texture1, GLuint texture2);

	/// @brief Method gives the new image
	/// @param ifLeft to say which way to turn
	/// @return returns the image as GLuint
	GLuint action(bool ifLeft);
};
