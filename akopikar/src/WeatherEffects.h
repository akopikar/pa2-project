#pragma once
#include <fstream>
#include "Scene.h"
#include "ConsumingHouse.h"
#include "FarmingHouse.h"
#include "BuildManager.h"
#include "main.h"
#include "FileManager.h"
#include <time.h>
#include "Point.h"
using namespace std; 

class WeatherEffects : public Scene, public FileManager 
{
	vector<Point> destructionPoints;

	int currentTime;
	int defaultTime = 200;
public:

	WeatherEffects() 
	{
		currentTime = defaultTime;
		Columns = 18;
		Rows = 10;
	}

	/// @brief Method saves current game values to the file
	void saveGame() override;

	/// @brief Method loads game values from the file
	void loadGame() override;

	/// @brief Method load textures 
	void load() override;
	
	/// @brief Method to proceed weathereffects
	/// @param Towers is a vector of towers which are to make changes to 
	void weatherChange(vector<shared_ptr<Tower>> Towers);
	
	/// @brief Method provides the destruction of towers
	void destruction();

	/// @brief Method check is the tower has been destroyed
	/// @param x Position of the tower on X axis
	/// @param y Position of the tower o Y axis
	/// @return Returns true if the tower is destroyed, otherwise returns false
	bool checkIfDestroyed(int x, int y);

	/// @brief Method resets currentTime
	void reset();


};
