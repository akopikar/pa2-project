#pragma once
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include <string>
#include <iostream>
#include <list>
#include <vector>
#include <sstream>

/// @brief Class is a parent class of Towers to be build
class Tower 
{
public: 
	string type;
	int posX = 0;
	int posY = 0;
	int cost = 0;
	int changeValue = 0;
	GLuint Texture = 0;

	~Tower() {}
	
	/// @brief Method returns action of the Tower
	/// @return returns outcome of the Tower 
	virtual int action() { return 0; };

	/// @brief Method resets playtime
	void timeReset() {
	}

	/// @brief Method gets a class as a string 
	/// @return string of class
	string getAsText() {
		stringstream ss;
		ss << type << " " << posX << " " << posY << " " << Texture << " " << changeValue << " " << cost << " " << endl;
		return ss.str();
	}

};
