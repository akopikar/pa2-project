#include  "inGame.h"

void InGame::load() 
{
    LoadTexture("textures/Game/gemP.jpg", 0);
    LoadTexture("textures/Game/gemPBack.jpg", 1);
    
    LoadTexture("textures/Game/house1.jpg", 2);
    LoadTexture("textures/Game/house2.jpg", 3);
    LoadTexture("textures/Game/house3.jpg", 4);

    LoadTexture("textures/Game/farm1_2.jpg", 5);
    LoadTexture("textures/Game/farm2_6.jpg", 6);
    LoadTexture("textures/Game/farm3_12.jpg", 7);

    weather.load();
    supp.load();
}

void InGame::saveGame() 
{
    ofstream file{ "save.bin" }; 
    
    file << GetGameTime() << endl;
    file << GetBestGameTime() << endl;
    file << money << endl;
    
    for (size_t i = 0; i < Towers.size(); i++)
    {
        file << Towers[i]->getAsText();
    }

    weather.saveGame();
}

void InGame::loadGame() 
{
    ifstream file{ "save.bin" }; 

    if (file.fail() || file.eof()) 
    {
        throw invalid_argument("No file exists");
    }
    unsigned tmpNum;

    // Game Time value
    file >> tmpNum;
    SetGameTime(tmpNum);

    // Best Time value
    file >> tmpNum;
    SetBestGameTime(tmpNum);

    // Money value
    file >> money;

    string tmp;
    string type;

    while(!file.eof())
    {
        file >> type;
        
        getline(file, tmp);

        if (type == "Farming") 
        {
            shared_ptr<FarmingHouse> farmingHouse(new FarmingHouse(tmp));
            Towers.push_back(farmingHouse);
        }
        else if (type == "Consuming") 
        {
            shared_ptr<ConsumingHouse> consumingHouse(new ConsumingHouse(tmp));
            Towers.push_back(consumingHouse);
        }

    }

    weather.loadGame();

}

void InGame::keyboard(unsigned char key) 
{
    if (key == 's') 
    {
        saveGame();
    }
    else if (key == 'q') 
    {
        EndGame();
    }
}

void InGame::menu() 
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    drawBackground();

    drawTextures();
    if (firstTime == true) 
    {
        drawGrid();
    }

    doActions();

    checkEndGame();


}

void InGame::reset() 
{
    money = defaultMoney;
    time = defaultTime;
    Towers.clear();
}

void InGame::checkEndGame() 
{
    if (money < 0 || 
        money > maxMoney || 
        Towers.size() == (Columns - 2) * (Rows - 2)) 
    {
        EndGame();
    }
}

void InGame::doActions() 
{
    time--;
    if (time <= 0) 
    {
        int num = 0;
        for (size_t i = 0; i < Towers.size(); i++)
        {
            if(!weather.checkIfDestroyed(Towers[i]->posX, Towers[i]->posY))
                num += Towers[i]->action();
        }

        if (num < money)
            supp.swap(false);
        else
            supp.swap(true);

        money += num;

        time = defaultTime;
    }

}

void InGame::drawTextures() 
{
    drawResources();
    drawTowersToBuy();
    drawAllTowers();
    weather.weatherChange(Towers);
    supp.menu();

}

void InGame::drawAllTowers() 
{
    for (size_t i = 0; i < Towers.size(); i++)
    {
        drawTex(Towers[i]->posX, Towers[i]->posY, Towers[i]->Texture);
        Towers[i]->action();
    }
}

void InGame::drawResources() 
{
    for (size_t i = 0; i < Columns; i++)
    {
        if (i < money) {
            drawTex(i, Rows - 1, Textures[0]);
        }
        else {
            drawTex(i, Rows - 1, Textures[1]);
        }
    }
}

void InGame::drawGrid() {
    glLineWidth(1.0);
   
    for (size_t x  = 0; x < Columns; x++)
    {
        for (size_t y = 0; y < Rows; y++)
        {

            if (x != 0 && y != 0 && x != Columns - 1 && y != Rows - 1)
            {
                glColor3f(1.0, 0.0, 0.0);
                glBegin(GL_LINE_LOOP);
                glVertex2f((GLfloat)x, (GLfloat)y);
                glVertex2f((GLfloat)(x + 1), (GLfloat)y);
                glVertex2f((GLfloat)(x + 1), (GLfloat)(y + 1));
                glVertex2f((GLfloat)x, (GLfloat)(y + 1));
                glEnd();
            }

            if (HouseToBuild != nullptr && HouseToBuild->posX == x && HouseToBuild->posY == y)
            {
                glColor3f(1.0, 0.0, 0.0);
                glLineWidth(5);
                glBegin(GL_LINE_LOOP);
                glVertex2f((GLfloat)x, (GLfloat)y);
                glVertex2f((GLfloat)(x + 1), (GLfloat)y);
                glVertex2f((GLfloat)(x + 1), (GLfloat)(y + 1));
                glVertex2f((GLfloat)x, (GLfloat)(y + 1));
                glEnd();
                glLineWidth(1.0);
            }
        }
    }

    glFlush();

}

void InGame::drawBackground() 
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glColor3f(0.7, 0.5, 1.0);

    glBegin(GL_QUADS);
    glVertex2f(0, 0);
    glVertex2f(Columns, 0);
    glColor3f(.9, .7, 1);
    glVertex2f(Columns, Rows);
    glVertex2f(0, Rows);
    glEnd();

    glFlush();
}

void InGame::drawTowersToBuy() 
{
    drawTex(4, 0, Textures[2]);
    drawTex(5, 0, Textures[3]);
    drawTex(6, 0, Textures[4]);

    drawTex(11, 0, Textures[5]);
    drawTex(12, 0, Textures[6]);
    drawTex(13, 0, Textures[7]);
}

void InGame::mouse(int button, int state, int x, int y) 
{
    int const w = glutGet(GLUT_WINDOW_WIDTH);
    int const h = glutGet(GLUT_WINDOW_HEIGHT);

    int mousePosX = x / (w/Columns);
    int mousePosY = (h - y) / (h / Rows);

    // Right click for selling towers
    if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) 
    {
        if(!weather.checkIfDestroyed(mousePosX, mousePosY))
            sellIfPossibe(mousePosX, mousePosY);
    }
    // Left click logic
    else if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) 
    {
        // To build a tower
        if (HouseToBuild != nullptr) 
        {
            if (HouseToBuild->cost <= money && 
                mousePosX != 0 && mousePosY != 0 &&
                mousePosX != Columns - 1 && mousePosY != Rows - 1) 
            {
                if (checkIfTaken(mousePosX, mousePosY) &&
                    weather.checkIfDestroyed(mousePosX, mousePosY))
                    return;

                HouseToBuild->posX = mousePosX;
                HouseToBuild->posY = mousePosY;
                Towers.push_back(HouseToBuild);
                money -= HouseToBuild->cost;
                HouseToBuild = nullptr;
            }
        }

        if (mousePosX == 4 && mousePosY == 0) 
        {
            shared_ptr<ConsumingHouse> towerToBuild(new ConsumingHouse(3, 1, Textures[2],4,0));
                HouseToBuild = towerToBuild;
        }
        else if (mousePosX == 5 && mousePosY == 0) 
        {
            shared_ptr<ConsumingHouse> towerToBuild(new ConsumingHouse(5, 2, Textures[3],5,0));
            HouseToBuild = towerToBuild;

        }
        else if (mousePosX == 6 && mousePosY == 0) 
        {
            shared_ptr<ConsumingHouse> towerToBuild(new ConsumingHouse(10, 3, Textures[4],6,0));
            HouseToBuild = towerToBuild;

        } 
       
        else if (mousePosX == 11 && mousePosY == 0) 
        {
            shared_ptr<FarmingHouse> towerToBuild(new FarmingHouse(2, 1, Textures[5],11,0));
            HouseToBuild = towerToBuild;

        }
        else if (mousePosX == 12 && mousePosY == 0) 
        {
            shared_ptr<FarmingHouse> towerToBuild(new FarmingHouse(6, 3, Textures[6],12,0));
            HouseToBuild = towerToBuild;

        }
        else if (mousePosX == 13 && mousePosY == 0) 
        {
            shared_ptr<FarmingHouse> towerToBuild(new FarmingHouse(12, 6, Textures[7],13,0));
            HouseToBuild = towerToBuild;

        }


    }
}

bool InGame::checkIfTaken(int x, int y) 
{
    for (size_t i = 0; i < Towers.size(); i++)
    {
        if (Towers[i]->posX == x && Towers[i]->posY == y) {
            return true;
        }
    }

    return false;
}

void InGame::sellIfPossibe(int x, int y) 
{
    for (size_t i = 0; i < Towers.size(); i++)
    {
        if (Towers[i]->posX == x && Towers[i]->posY == y) {
            money += Towers[i]->cost / 2;
            Towers.erase(Towers.begin() + i);
        }
    }

}