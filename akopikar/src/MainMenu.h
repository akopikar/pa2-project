#pragma once

#include "Scene.h"
#include "main.h"
#include "DisplayRenderer.h"

class MainMenu :public Scene, public DisplayRenderer
{

public: 
    /// @brief Constructor
    MainMenu() 
    {
        Columns = 5;
        Rows = 5;
    }

    /// @brief Method load textures 
    void load();

    /// @brief Method runs render function
    void menu() override;

    /// @brief Method draws Buttons to the graphics window
    void drawButtons();

    /// @brief Method draws Background to the graphics window
    void drawBackground();

    /// @brief Method is used to manipulate mouse inputs
    /// @param button is a character pressed
    /// @param state is OpenGl keyboard state
    /// @param x is a point of the window on X axis where mouse is pointing 
    /// @param y is a point of the window on Y axis where mouse is pointing
    void mouse(int button, int state, int x, int y) override;

};