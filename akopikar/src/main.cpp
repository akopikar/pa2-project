#include "main.h"
#include "MainMenu.h"
#include "inGame.h"

const int FPS = 30;

/// @brief Timer for the game
unsigned gameTime = 0;

/// @brief Best scored time
unsigned bestTime = 0;

/// @brief MainMenu scene
MainMenu menu;

/// @brief InGame scene
InGame inGame;

/// @brief Time getter
/// @return Time
unsigned GetGameTime() 
{
    return gameTime;
}

/// @brief Best Time getter
/// @return Best Time
unsigned GetBestGameTime() 
{
    return bestTime;
}

/// @brief Time setter
/// @param time Time to set
void SetGameTime(unsigned time) 
{
    gameTime = time;
}

/// @brief Best Time setter
/// @param time Time to set
void SetBestGameTime(unsigned time) 
{
    bestTime = time;
}

/// @brief OpenGl function to reshape the window
/// @param w Width of the window
/// @param h Height of the window
void reshape(int w, int h) 
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
   
    switch (GameState)
    {
    case gameState::MainMenu:
        glOrtho(0.0, menu.Columns, 0.0, menu.Rows, -1.0, 1.0);
        break;
    case gameState::InGame:
        glOrtho(0.0, inGame.Columns, 0.0, inGame.Rows, -1.0, 1.0);
        break;
    default:
        break;
    }

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

}

/// @brief OpenGl function for rendering
void renderFunction()
{
    switch (GameState)
    {
    case gameState::MainMenu:
        menu.menu();
        break;
    case gameState::InGame:
        gameTime++;
        inGame.menu();
        break;
    default:
        break;
    }
}

/// @brief Method is used for keyboard inputs (OpenGl)
/// @param key is a value pressed by the player 
/// @param  
/// @param  
void keyboardFunction(unsigned char key, int, int)
{
    switch (GameState)
    {
    case gameState::MainMenu:
        menu.keyboard(key);
        break;
    case gameState::InGame:
        inGame.keyboard(key);
        break;
    default:
        break;
    }
}

/// @brief Method is used to manipulate mouse inputs
/// @param button is a character pressed
/// @param state is OpenGl keyboard state
/// @param x is a point of the window on X axis where mouse is pointing 
/// @param y is a point of the window on Y axis where mouse is pointing
void mouseFunction(int button, int state, int x, int y)
{
    switch (GameState)
    {
    case gameState::MainMenu:
        menu.mouse(button, state, x, y);
        break;
    case gameState::InGame:
        inGame.mouse(button, state, x, y);
        break;
    default:
        break;
    }
}

void StartGame() 
{
    int const w = glutGet(GLUT_WINDOW_WIDTH);
    int const h = glutGet(GLUT_WINDOW_HEIGHT);
    GameState = gameState::InGame;
    reshape(w, h);
}

void LoadGame() 
{
    inGame.loadGame();
    StartGame();
}

/// @brief Method is used to get the current time
/// @return Number in seconds 
unsigned getTime() 
{
    unsigned timeNow = gameTime / FPS;

    if (bestTime < timeNow)
        bestTime = timeNow;

    return timeNow;
}

void EndGame() 
{
    int const w = glutGet(GLUT_WINDOW_WIDTH);
    int const h = glutGet(GLUT_WINDOW_HEIGHT);

    GameState = gameState::MainMenu;
    inGame.reset();

    reshape(w, h);

    cout << "Your time is: " << getTime() << ", your best time is: " << bestTime <<endl;
    gameTime = 0;
}

/// @brief OpenGl method
/// @param  
void timer(int) 
{
    glutPostRedisplay();
    glutTimerFunc(1000 / FPS, timer, 0);
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE | GLUT_DEPTH);
    glutInitWindowSize(1800, 1000);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Game");

    menu.load();
    inGame.load();

    glutReshapeFunc(reshape);
    glutDisplayFunc(renderFunction);
    glutKeyboardFunc(keyboardFunction);
    glutMouseFunc(mouseFunction);
    glutTimerFunc(0, timer, 0);
    glutMainLoop(); 
    return 0;
}