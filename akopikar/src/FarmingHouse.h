#pragma once
#include "Tower.h"

class FarmingHouse : public Tower 
{
public:
	/// @brief Constructor
	/// @param costs Tower cost
	/// @param changer Tower action value
	/// @param Tex Tower texture
	/// @param x Tower X position
	/// @param y Tower Y position
	FarmingHouse(int costs, int changer, GLuint Tex, int x, int y) 
	{
		type = "Farming";
		posX = x;
		posY = y;
		Texture = Tex;
		changeValue = changer;
		cost = costs;
	}

	/// @brief Constructor to load from string
	/// @param str1 String form which to store tower values
	FarmingHouse(string str1) 
	{
		stringstream str(str1);
		type = "Farming";
		str >> posX;
		str >> posY;
		str >> Texture;
		str >> changeValue;
		str >> cost;
	}


	int action() override 
	{
		return changeValue;
		
	}


};