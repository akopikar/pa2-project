#include "WeatherEffects.h"

void WeatherEffects::load() 
{
	LoadTexture("textures/Weather/fire.jpg", 0);
}

void WeatherEffects::saveGame() 
{
	ofstream file{ "saveWeather.bin" };
	file << currentTime;
	for (size_t i = 0; i < destructionPoints.size(); i++)
	{
		file << destructionPoints[i].x << " " << destructionPoints[i].y << endl;
	}
}

void WeatherEffects::loadGame() 
{
	ifstream file{ "saveWeather.bin" };
	if (file.fail() || file.eof())
	{
		throw invalid_argument("No file exists");
	}
	file >> currentTime;

	while (!file.eof())
	{
		int x, y;
		file >> x >> y;

		Point point;
		point.x = x;
		point.y = y;

		destructionPoints.push_back(point);
	}
}

void WeatherEffects::weatherChange(vector<shared_ptr<Tower>> Towers) {
	if (Towers.size() < 3) return;
	currentTime--;
	
	if (currentTime <= 0) 
	{
		srand(time(NULL));
		int randomNumber = rand() % Towers.size();

		Point point;
		point.x = Towers[randomNumber]->posX;
		point.y = Towers[randomNumber]->posY;

		destructionPoints.push_back(point);

		reset();
	}

	destruction();
}

void WeatherEffects::destruction() 
{
	for (size_t i = 0; i < destructionPoints.size(); i++)
	{
		drawTex(destructionPoints[i].x, destructionPoints[i].y, Textures[0]);
	}
}

bool WeatherEffects::checkIfDestroyed(int x, int y) 
{
	for (size_t i = 0; i < destructionPoints.size(); i++)
	{
		if (x == destructionPoints[i].x && destructionPoints[i].y == y)
			return true;
	}

	return false;
}

void WeatherEffects::reset() 
{
	currentTime = defaultTime;
}
