#pragma once

#include <fstream>
#include "Scene.h"
#include "ConsumingHouse.h"
#include "FarmingHouse.h"
#include "BuildManager.h"
#include "main.h"
#include "WeatherEffects.h"
#include "FileManager.h"
#include "DisplayRenderer.h"
#include "Supporter.h"

/// @brief InGame class is a Scene with the gameplay
class InGame :public Scene, public FileManager, public DisplayRenderer
{
    vector<shared_ptr<Tower>> Towers;

    int money = 0; 
    int defaultMoney = 2; 
    int defaultTime = 100; 
    int maxMoney = 18;
    int time = defaultTime;
    bool firstTime = true;
    WeatherEffects weather;
    Supporter supp;
public:
    /// @brief Constructor
    InGame() 
    {
        Columns = 18;
        Rows = 10;
        money = defaultMoney;
    }


    /// @brief Method load textures 
    void load() override;

    /// @brief Method runs render function
    void menu() override;

    /// @brief Method resets values of the class
    void reset();

    /// @brief Method draws available Towers To Buy to the graphics window
    void drawTowersToBuy();

    /// @brief Method draws Resources to the graphics window
    void drawResources();

    /// @brief Method draws Textures to the graphics window
    void drawTextures();

    /// @brief Method draws Background to the graphics window
    void drawBackground();

    /// @brief Method draws Grid to the graphics window
    void drawGrid();

    /// @brief Method runs actions of every Tower
    void doActions();

    /// @brief Method draws all Towers to the graphics window
    void drawAllTowers();

    /// @brief Method sells a Tower if is exists
    /// @param x Index of Tower on X axis 
    /// @param y Index of Tower on Y axis 
    void sellIfPossibe(int x, int y);

    /// @brief Method check if there is any Tower on given point
    /// @param x Index of Tower on X axis 
    /// @param y Index of Tower on Y axis 
    /// @return Returns true if there is a Tower on a given point
    bool checkIfTaken(int x, int y);

    /// @brief Method checks the end game requirements
    void checkEndGame();

    /// @brief Method saves current game values to the file
    void saveGame() override;

    /// @brief Method loads game values from the file
    void loadGame() override;

    /// @brief Method is used to manipulate mouse inputs
    /// @param button is a character pressed
    /// @param state is OpenGl keyboard state
    /// @param x is a point of the window on X axis where mouse is pointing 
    /// @param y is a point of the window on Y axis where mouse is pointing
    void mouse(int button, int state, int x, int y) override;

    /// @brief Method is used for keyboard inputs
    /// @param key is a value pressed by the player
    void keyboard(unsigned char key ) override;

};

