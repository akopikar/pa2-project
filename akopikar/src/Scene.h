#pragma once
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <iostream>
using namespace std;


class Scene 
{
public :
    int Columns = 0;
    int Rows = 0;

    GLuint Textures[8] = {0};
    
    virtual ~Scene() { 
        
    }

    /// @brief Method loads Textures form file
    /// @param fname Path of the texture
    /// @param id Id of the texture
    void LoadTexture(string fname, int id);

    /// @brief Method draws Texture 
    /// @param x X value of the grid
    /// @param y Y value of the grid
    /// @param Texture Texture to be drawn
    void drawTex(int x, int y, GLuint Texture);

    /// @brief Method load textures 
    virtual void load() {}
    
    /// @brief Method is used for keyboard inputs
    /// @param key is a value pressed by the player
    virtual void keyboard(unsigned char key ) {}

    /// @brief Method is used to manipulate mouse inputs
    /// @param button is a character pressed
    /// @param state is OpenGl keyboard state
    /// @param x is a point of the window on X axis where mouse is pointing 
    /// @param y is a point of the window on Y axis where mouse is pointing
    virtual void mouse(int button, int state, int x, int y) {}


};

