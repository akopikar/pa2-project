#pragma once
#include "Scene.h"
#include "Animator.h"
#include "DisplayRenderer.h"

class Supporter :public Scene, public DisplayRenderer
{
    Animator anim;
    GLuint tex;
public:
    Supporter() {
        Columns = 18;
        Rows = 10;
    }
    /// @brief Method load textures 
    void load() override;

    /// @brief Method runs render function
    void menu() override;

    /// @brief Method swaps sides of the image
    /// @param ifLeft to say which way to turn
    void swap(bool ifLeft);
};