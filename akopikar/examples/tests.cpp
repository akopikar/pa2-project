#include "tests.h"

void Tests::ConsumingHouseTests() {
	shared_ptr<Tower> tower;

	shared_ptr<ConsumingHouse> towerToBuild(new ConsumingHouse(3, 1, Textures[2], 5, 0));
	tower = towerToBuild;

	assert(tower->getAsText(), "Consuming 3 1 2 5 0\n");

}

void Tests::FarmingHouseTests() {
	shared_ptr<Tower> tower;

	shared_ptr<FarmingHouse> towerToBuild(new FarmingHouse(2, 1, Textures[5], 9, 0));
	tower = towerToBuild;

	assert(tower->getAsText(), "Farming 2 1 5 9 0\n");
}


void Tests::WeatherEffectsTests() {
	vector < shared_ptr<Tower>> tower;
	shared_ptr<ConsumingHouse> towerToBuild1(new ConsumingHouse(1, 0, Textures[2], 5, 0));
	shared_ptr<ConsumingHouse> towerToBuild2(new FarmingHouse(2, 0, Textures[2], 5, 0));
	shared_ptr<ConsumingHouse> towerToBuild3(new ConsumingHouse(3, 0, Textures[2], 5, 0));

	tower.push_back(towerToBuild1);
	tower.push_back(towerToBuild2);
	tower.push_back(towerToBuild3);

	WeatherEffects effect;
	
	for (size_t i = 0; i < 201; i++)
	{
		effect.weatherChange(tower);
	}

	assert(effect.checkIfDestroyed(1,0) ||
		effect.checkIfDestroyed(2, 0) || 
		effect.checkIfDestroyed(3,0),true);
}
