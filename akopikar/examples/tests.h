#include "../src/Tower.h"
#include "../src/ConsumingHouse.h"
#include "../src/FarmingHouse.h"
#include "../src/inGame.h"
#include "../src/Tower.h"
#include "../src/WeatherEffects.h"
#include <memory>
#include <assert.h>
using namespace std;

class Tests {

	void ConsumingHouseTests();
	void FarmingHouseTests();
	void WeatherEffectsTests();

};
