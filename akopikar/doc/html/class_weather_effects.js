var class_weather_effects =
[
    [ "WeatherEffects", "class_weather_effects.html#a36271ad36890c2fe1d22401f9cb6765d", null ],
    [ "checkIfDestroyed", "class_weather_effects.html#a61beca1a327d6f28affada775225ab31", null ],
    [ "destruction", "class_weather_effects.html#a50148833e8f4e4efab93f98c7143e167", null ],
    [ "load", "class_weather_effects.html#ab602e85dbb62e7d1dc4e2b88220b960d", null ],
    [ "loadGame", "class_weather_effects.html#a1733b8f22fa9f7b233333bf435e2dafe", null ],
    [ "reset", "class_weather_effects.html#a6dff5e84c96b48a4391ecb7dad76ff81", null ],
    [ "saveGame", "class_weather_effects.html#a68aef476031b35c31d7e3a437ffcb02e", null ],
    [ "weatherChange", "class_weather_effects.html#a8388dd4cb27cb06add5c6f892c167a77", null ]
];